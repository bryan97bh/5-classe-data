package data;

public class Teste {

	public static void main(String[] args) {
		Data data = new Data(29, 2, 2020);
		System.out.println(data);
		
		
		System.out.println();
		
		for(int i = 0; i < 90; i++) {
			data.incrementaDia();
			System.out.println(data);
		}
		
		System.out.println();
		
		for(int i = 0; i < 12; i++) {
			data.incrementaMes();
			System.out.println(data);
		}
		
		System.out.println();
		
		for(int i = 0; i < 15; i++) {
			data.incrementaAno();
			System.out.println(data);
		}

	}
	
}
