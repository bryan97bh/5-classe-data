package data;

public class Data {

	private byte dia; // {1 .. {28, 29, 30 ou 31} }
	private byte mes; // {1 .. 12}
	private short ano; // {1 .. 9999}

	private boolean ehBissexto(short ano) {
		return (ano % 400 == 0) || ((ano % 4 == 0) && (ano % 100 != 0));
	}

	private byte getUltimoDia(byte mes, short ano) {
		byte ud[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

		if (mes == 2 && ehBissexto(ano)) {
			return 29;
		}

		return ud[mes];
	}

	public Data() {
		setAno((byte) 1);
		setMes((byte) 1);
		setDia((byte) 1);
	}

	public Data(int dia, int mes, int ano) {
		this();
		setAno((short) ano);
		setMes((byte) mes);
		setDia((byte) dia);
	}

	public byte getDia() {
		return dia;
	}

	public void setDia(byte dia) {
		byte ultimoDia = getUltimoDia(mes, ano);

		if (dia >= 1 && dia <= ultimoDia) {
			this.dia = dia;
		}
	}

	public byte getMes() {
		return mes;
	}

	public void setMes(byte mes) {
		if (mes >= 1 && mes <= 12) {
			this.mes = mes;
		}
	}

	public short getAno() {
		return ano;
	}

	public void setAno(short ano) {
		if (ano >= 1 && ano <= 9999) {
			this.ano = ano;
		}
	}

	public void incrementaDia() {
		byte ultimoDia = getUltimoDia(mes, ano);

		byte aux = (byte) (dia + 1);

		if (aux == (ultimoDia + 1)) {
			dia = 1;
			incrementaMes();
		} else {
			dia = aux;
		}
	}

	public void incrementaMes() {
		byte aux = (byte) (mes + 1);

		if (aux == 13) {
			mes = 1;
			incrementaAno();
		} else {
			mes = aux;
		}
	}

	public void incrementaAno() {
		short aux = (short) (ano + 1);

		if (aux == 10000) {
			ano = 1;
		} else {
			ano = aux;
		}
	}

	@Override
	public String toString() {
		return getDia() + "/" + getMes() + "/" + getAno();
	}
}
